---
title: "Monado - Dependencies and Requirements"
layout: main
---

* TOC
{:toc}

# Monado Requirements

## Graphics Driver Requirements.

The Monado compositor requires a Vulkan driver with the instance extensions:

* VK_KHR_external_fence_capabilities
* VK_KHR_external_memory_capabilities
* VK_KHR_external_semaphore_capabilities
* VK_KHR_get_physical_device_properties2
* VK_KHR_surface
* for direct mode on Linux/X11
  * VK_KHR_display
  * VK_EXT_direct_mode_display
  * VK_EXT_acquire_xlib_display

and the Device extensions:

* VK_KHR_dedicated_allocation
* VK_KHR_external_fence
* VK_KHR_external_fence_fd
* VK_KHR_external_memory
* VK_KHR_external_memory_fd
* VK_KHR_external_semaphore
* VK_KHR_external_semaphore_fd
* VK_KHR_get_memory_requirements2
* VK_KHR_swapchain

OpenXR applications using Vulkan are supported with all Vulkan drivers that support the listed extensions. In particular radv, intel anv and the nvidia proprietary driver are tested and confirmed to work.

OpenXR applications using OpenGL require an OpenGL driver with support for the `GL_EXT_memory_object_fd` OpenGL extension. OpenGL applications are supported with radeonsi and the nvidia proprietary driver.\\
[Intel does not currently support this extension in mainline mesa](https://gitlab.freedesktop.org/mesa/mesa/-/issues/1824) but there are WIP MRs [for mesa/iris](https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/4337) and [for mesa/i965](https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/5594) that have been tested to work with Monado.

Running Monado with the amdvlk Vulkan driver generally works but may not render OpenXR applications using OpenGL correctly.

# Optional dependencies

## Leap Motion

Leap Motion does not ship cmake or pkg-config files. The path to the Leap Motion SDK can be given to cmake with the argument

`-DLeapV2_ROOT_DIR=/path/to/LeapDeveloperKit_2.3.1+31549_linux/LeapSDK/`

## Intel Realsense

If librealsense is installed in a nonstandard path (e.g. compiled with -DCMAKE_INSTALL_PREFIX=...) the path to its install prefix can be given to cmake via standard cmake facilities:

`-Drealsense2_DIR=/path/to/install/prefix`
